package com.tgelo.loanservice.api.rest;

import com.tgelo.loanservice.api.dto.ErrorResponse;
import com.tgelo.loanservice.domain.error.Error;
import com.tgelo.loanservice.domain.error.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
class DefaultExceptionHandler {

  @ExceptionHandler(HttpMessageConversionException.class)
  ResponseEntity<ErrorResponse> invalidRequest(final HttpMessageConversionException ex) {
    log.info("Invalid request", ex);
    return toErrorResponse(ErrorCode.INVALID_REQUEST.asError());
  }

  @ExceptionHandler
  ResponseEntity<ErrorResponse> handleException(final Throwable ex) {
    log.error("Unknown exception", ex);
    return toErrorResponse(ErrorCode.INTERNAL_ERROR.asError());
  }

  private ResponseEntity<ErrorResponse> toErrorResponse(final Error error) {
    return ResponseEntity.status(error.getStatus())
      .body(ErrorResponse.of(error));
  }
}
