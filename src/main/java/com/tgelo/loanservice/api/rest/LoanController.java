package com.tgelo.loanservice.api.rest;

import com.tgelo.loanservice.api.dto.CreateLoanRequest;
import com.tgelo.loanservice.api.dto.LoanHistoryItemResponse;
import com.tgelo.loanservice.api.dto.LoanResponse;
import com.tgelo.loanservice.business.CreateLoanCommand;
import com.tgelo.loanservice.business.ExtendLoanCommand;
import com.tgelo.loanservice.business.LoanOperations;
import com.tgelo.loanservice.domain.TimeProvider;
import com.tgelo.loanservice.domain.loan.Loan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class LoanController extends CommonController {

  public static final String API_V1_LOANS = API_V1 + "/loans";
  public static final String API_V1_LOAN_EXTEND = API_V1_LOANS + "/{id}/extend";

  private final LoanOperations loanOperations;

  LoanController(final LoanOperations loanOperations) {
    this.loanOperations = loanOperations;
  }

  @PostMapping(API_V1_LOANS)
  ResponseEntity create(@RequestBody final CreateLoanRequest request) {
    log.debug("Create loan: {}", request);
    final CreateLoanCommand command = CreateLoanCommand.builder()
      .amount(request.getAmount())
      .currency(request.getCurrency())
      .term(request.getTerm())
      .createdAt(TimeProvider.instantNow())
      .build();

    return loanOperations.create(command)
      .map(this::toResponse)
      .fold(this::toErrorResponse, dto -> toCreatedResponse(dto, dto.getId()));
  }

  @PostMapping(API_V1_LOAN_EXTEND)
  ResponseEntity extend(@PathVariable final Long id) {
    log.debug("Extend loan with id: {}", id);
    final ExtendLoanCommand command = ExtendLoanCommand.of(id);

    return loanOperations.extend(command)
      .map(this::toResponse)
      .fold(this::toErrorResponse, ResponseEntity::ok);
  }

  private LoanResponse toResponse(final Loan loan) {
    return LoanResponse.builder()
      .id(loan.getId())
      .amount(loan.getAmount())
      .cost(loan.cost())
      .rate(loan.getRate())
      .currency(loan.getCurrency())
      .createdAt(loan.getCreatedAt())
      .history(loan.getHistory().map(h ->
        LoanHistoryItemResponse.builder()
          .id(h.getId())
          .action(h.getAction())
          .loanId(h.getLoanId())
          .totalTerm(h.getTotalTerm())
          .createdAt(h.getCreatedAt())
          .dueDate(h.getDueDate())
          .build()
      ))
      .totalTerm(loan.totalTerm())
      .dueDate(loan.dueDate())
      .build();
  }
}
