package com.tgelo.loanservice.api.rest;

import com.tgelo.loanservice.api.dto.ErrorResponse;
import com.tgelo.loanservice.domain.error.Error;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

abstract class CommonController {

  static final String API_V1 = "/api/v1";

  ResponseEntity<ErrorResponse> toErrorResponse(final Error error) {
    return ResponseEntity
      .status(error.getStatus())
      .body(ErrorResponse.of(error));
  }

  ResponseEntity toCreatedResponse(final Object responseBody, final Object responseId) {
    return ResponseEntity.created(
      ServletUriComponentsBuilder.fromCurrentRequestUri()
        .pathSegment(responseId.toString())
        .build()
        .toUri()
    ).body(responseBody);
  }
}
