package com.tgelo.loanservice.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.Wither;

import java.util.Currency;

@Wither
@Value
@Builder
public class CreateLoanRequest {

  @JsonProperty("amount")
  @NonNull
  Long amount;

  @JsonProperty("currency")
  @NonNull
  Currency currency;

  @JsonProperty("term")
  @NonNull
  Long term;
}
