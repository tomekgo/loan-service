package com.tgelo.loanservice.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tgelo.loanservice.domain.error.Error;
import com.tgelo.loanservice.domain.error.ErrorCodeType;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

@Wither
@Builder
@Value
public class ErrorResponse {

  @JsonProperty("type")
  ErrorCodeType type;

  @JsonProperty("status")
  int status;

  @JsonProperty("code")
  String code;

  @JsonProperty("arguments")
  List<String> arguments;

  @JsonProperty("message")
  String message;

  public static ErrorResponse of(final Error error) {
    return ErrorResponse.builder()
      .type(error.getType())
      .status(error.getStatus())
      .code(error.getCode())
      .arguments(error.getArguments())
      .message(error.getMessage())
      .build();
  }
}
