package com.tgelo.loanservice.api.dto;

import com.tgelo.loanservice.domain.loan.LoanAction;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import java.time.Instant;

@Wither
@Builder
@Value
public class LoanHistoryItemResponse {

  Long id;

  Instant dueDate;

  Long totalTerm;

  LoanAction action;

  Long loanId;

  Instant createdAt;
}
