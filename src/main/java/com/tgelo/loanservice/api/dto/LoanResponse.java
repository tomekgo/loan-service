package com.tgelo.loanservice.api.dto;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import java.time.Instant;
import java.util.Currency;

@Wither
@Builder
@Value
public class LoanResponse {

  Long id;

  Long amount;

  Long cost;

  Integer rate;

  Currency currency;

  Instant createdAt;

  List<LoanHistoryItemResponse> history;

  Long totalTerm;

  Instant dueDate;
}
