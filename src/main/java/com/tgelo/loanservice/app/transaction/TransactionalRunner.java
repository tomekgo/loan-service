package com.tgelo.loanservice.app.transaction;

import io.vavr.control.Validation;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.tgelo.loanservice.domain.error.Error;

import java.util.function.Supplier;

@Component
public class TransactionalRunner {

  @Transactional(rollbackFor = Throwable.class)
  public <T> Validation<Error, T> doInTransaction(final Supplier<Validation<Error, T>> supplier) {
    return supplier.get();
  }
}
