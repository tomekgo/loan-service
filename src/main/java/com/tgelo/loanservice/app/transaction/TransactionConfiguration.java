package com.tgelo.loanservice.app.transaction;

import io.vavr.control.Validation.Invalid;
import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.NoTransactionException;
import org.springframework.transaction.annotation.ProxyTransactionManagementConfiguration;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.interceptor.TransactionInterceptor;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;

@Configuration
public class TransactionConfiguration extends ProxyTransactionManagementConfiguration {

  @Override
  public TransactionInterceptor transactionInterceptor() {
    final var interceptor = new VavrAwareTransactionInterceptor();
    interceptor.setTransactionAttributeSource(transactionAttributeSource());
    interceptor.setTransactionManager(txManager);
    return interceptor;
  }

  private static class VavrAwareTransactionInterceptor extends TransactionInterceptor {

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
      return super.invoke(new RollingBackOnInvalidMethodInvocation(invocation));
    }
  }

  @Slf4j
  private static class RollingBackOnInvalidMethodInvocation implements MethodInvocation {

    final MethodInvocation wrapped;

    RollingBackOnInvalidMethodInvocation(MethodInvocation wrapped) {
      this.wrapped = wrapped;
    }

    @Override
    public Object proceed() throws Throwable {
      var returnValue = wrapped.proceed();
      if (returnValue instanceof Invalid) {
        try {
          TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        } catch (final NoTransactionException e) {
          log.error("Got to aspect-driven rollback but no aspect-managed transaction exists", e);
        }
      }
      return returnValue;
    }

    @Override
    public Method getMethod() {
      return wrapped.getMethod();
    }

    @Override
    public Object[] getArguments() {
      return wrapped.getArguments();
    }

    @Override
    public Object getThis() {
      return wrapped.getThis();
    }

    @Override
    public AccessibleObject getStaticPart() {
      return wrapped.getStaticPart();
    }
  }
}
