package com.tgelo.loanservice.persistence.converters;

import com.tgelo.loanservice.domain.loan.LoanAction;
import org.jooq.impl.EnumConverter;

public class LoanActionConverter extends EnumConverter<String, LoanAction> {

  public LoanActionConverter() {
    super(String.class, LoanAction.class);
  }
}
