package com.tgelo.loanservice.persistence.converters;

import lombok.NoArgsConstructor;
import org.jooq.Converter;

import java.util.Currency;

@NoArgsConstructor
public class CurrencyConverter implements Converter<String, Currency> {

  @Override
  public Currency from(final String currencyCode) {
    if (currencyCode == null) {
      return null;
    }
    return Currency.getInstance(currencyCode);
  }

  @Override
  public String to(final Currency currency) {
    if (currency == null) {
      return null;
    }
    return currency.getCurrencyCode();
  }

  @Override
  public Class<String> fromType() {
    return String.class;
  }

  @Override
  public Class<Currency> toType() {
    return Currency.class;
  }
}
