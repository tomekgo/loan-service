package com.tgelo.loanservice.persistence.converters;

import lombok.NoArgsConstructor;
import org.jooq.Converter;

import java.sql.Timestamp;
import java.time.Instant;

@NoArgsConstructor
public class InstantConverter implements Converter<Timestamp, Instant> {

  @Override
  public Instant from(final Timestamp timestamp) {
    if (timestamp == null) {
      return null;
    }
    return timestamp.toInstant();
  }

  @Override
  public Timestamp to(final Instant instant) {
    if (instant == null) {
      return null;
    }
    return Timestamp.from(instant);
  }

  @Override
  public Class<Timestamp> fromType() {
    return Timestamp.class;
  }

  @Override
  public Class<Instant> toType() {
    return Instant.class;
  }
}
