package com.tgelo.loanservice.persistence;

import com.tgelo.loanservice.domain.error.Error;
import com.tgelo.loanservice.domain.error.ErrorCode;
import io.vavr.control.Validation;
import org.jooq.exception.DataAccessException;
import org.slf4j.LoggerFactory;

interface JooqRepository {

  @FunctionalInterface
  interface ValidationSupplier<T> {
    Validation<Error, T> get();
  }

  default <T> Validation<Error, T> convertExceptions(final ValidationSupplier<T> supplier) {
    try {
      return supplier.get();
    } catch (final DataAccessException | org.springframework.dao.DataAccessException e) {
      LoggerFactory.getLogger(getClass()).error("Failed to execute query", e);
      return Validation.invalid(ErrorCode.DATA_ACCESS_EXCEPTION.asError());
    }
  }
}
