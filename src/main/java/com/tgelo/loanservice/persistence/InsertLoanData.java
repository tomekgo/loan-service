package com.tgelo.loanservice.persistence;

import com.tgelo.loanservice.domain.loan.LoanAction;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.Wither;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Currency;

@Builder
@Wither
@Value
public class InsertLoanData {

  @NonNull
  Long amount;

  @NonNull
  Integer rate;

  @NonNull
  Currency currency;

  @NonNull
  Instant createdAt;

  @NonNull
  LoanAction action;

  @NonNull
  Long term;

  Long cost() {
    return amount * rate / 100L;
  }

  Instant dueDate() {
    return createdAt.plus(term, ChronoUnit.DAYS);
  }
}
