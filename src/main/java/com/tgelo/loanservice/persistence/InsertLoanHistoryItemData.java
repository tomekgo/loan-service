package com.tgelo.loanservice.persistence;

import com.tgelo.loanservice.domain.loan.LoanAction;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.Wither;

import java.time.Instant;

@Builder
@Wither
@Value
public class InsertLoanHistoryItemData {

  @NonNull
  Long loanId;

  @NonNull
  LoanAction action;

  @NonNull
  Long totalTerm;

  @NonNull
  Instant dueDate;

  @NonNull
  Instant createdAt;
}
