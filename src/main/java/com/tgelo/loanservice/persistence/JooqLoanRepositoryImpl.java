package com.tgelo.loanservice.persistence;

import com.tgelo.loanservice.business.repositories.LoanRepository;
import com.tgelo.loanservice.domain.error.Error;
import com.tgelo.loanservice.domain.loan.Loan;
import com.tgelo.loanservice.domain.loan.LoanHistoryItem;
import com.tgelo.loanservice.tables.records.LoanHistoryItemsRecord;
import com.tgelo.loanservice.tables.records.LoansRecord;
import io.vavr.collection.List;
import io.vavr.control.Validation;
import lombok.extern.slf4j.Slf4j;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import static com.tgelo.loanservice.Tables.LOAN_HISTORY_ITEMS;
import static com.tgelo.loanservice.domain.error.ErrorCode.LOAN_DOES_NOT_EXIST;
import static com.tgelo.loanservice.tables.Loans.LOANS;
import static io.vavr.API.Invalid;
import static io.vavr.API.Valid;

@Slf4j
@Component
class JooqLoanRepositoryImpl implements LoanRepository, JooqRepository {

  private final DSLContext context;

  JooqLoanRepositoryImpl(final DSLContext context) {
    this.context = context;
  }

  @Override
  public Validation<Error, Loan> insert(final InsertLoanData insertData) {
    return convertExceptions(() -> {
      log.debug("Insert loan: {}", insertData);
      final LoansRecord record = context.newRecord(LOANS)
        .setAmount(insertData.getAmount())
        .setRate(insertData.getRate())
        .setCost(insertData.cost())
        .setCurrency(insertData.getCurrency())
        .setCreatedAt(insertData.getCreatedAt());

      record.store();
      return insert(
        InsertLoanHistoryItemData.builder()
          .loanId(record.getId())
          .action(insertData.getAction())
          .totalTerm(insertData.getTerm())
          .dueDate(insertData.dueDate())
          .createdAt(insertData.getCreatedAt())
          .build()
      );
    });
  }

  @Override
  public Validation<Error, Loan> insert(final InsertLoanHistoryItemData insertData) {
    return convertExceptions(() -> {
      try {
        log.debug("Insert loan history item: {}", insertData);
        final LoanHistoryItemsRecord record = context.newRecord(LOAN_HISTORY_ITEMS)
          .setLoanId(insertData.getLoanId())
          .setAction(insertData.getAction())
          .setTotalTerm(insertData.getTotalTerm())
          .setDueDate(insertData.getDueDate())
          .setCreatedAt(insertData.getCreatedAt());

        record.store();
        return retrieve(insertData.getLoanId());
      } catch (final DataIntegrityViolationException e) {
        log.debug("Loan with given id {} doesn't exist.", insertData.getLoanId());
        return Invalid(LOAN_DOES_NOT_EXIST.asError(insertData.getLoanId()));
      }
    });
  }

  @Override
  public Validation<Error, Loan> retrieve(final Long id) {
    return convertExceptions(() -> {
      log.debug("Retrieve loan with id: {}", id);
      final Result<Record> records = context
        .selectFrom(
          LOANS.leftJoin(LOAN_HISTORY_ITEMS)
            .on(LOANS.ID.eq(LOAN_HISTORY_ITEMS.LOAN_ID))
        )
        .where(LOANS.ID.eq(id))
        .orderBy(LOAN_HISTORY_ITEMS.ID)
        .fetch();

      if (records.isEmpty()) {
        log.debug("Loan with given id: {} doesn't exist.", id);
        return Invalid(LOAN_DOES_NOT_EXIST.asError(id));
      } else {
        final List<Record> listOfRecords = List.ofAll(records);
        final LoansRecord loan = listOfRecords.head().into(LOANS);
        final List<LoanHistoryItemsRecord> historyItems =
          listOfRecords
            .map(r -> r.into(LOAN_HISTORY_ITEMS))
            .filter(r -> r.getLoanId() != null);

        return Valid(toLoan(loan, historyItems));
      }
    });
  }

  private Loan toLoan(final LoansRecord loan, final List<LoanHistoryItemsRecord> historyItems) {
    return Loan.builder()
      .id(loan.getId())
      .amount(loan.getAmount())
      .rate(loan.getRate())
      .currency(loan.getCurrency())
      .createdAt(loan.getCreatedAt())
      .history(historyItems.map(h ->
        LoanHistoryItem.builder()
          .id(h.getId())
          .action(h.getAction())
          .loanId(h.getLoanId())
          .totalTerm(h.getTotalTerm())
          .createdAt(h.getCreatedAt())
          .dueDate(h.getDueDate())
          .build()
      ))
      .build();
  }
}
