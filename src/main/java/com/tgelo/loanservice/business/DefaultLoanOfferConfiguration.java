package com.tgelo.loanservice.business;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Configuration
@ConfigurationProperties("default-loan-offer")
@Validated
public class DefaultLoanOfferConfiguration {

  @NotNull
  private Long minAmount;

  @NotNull
  private Long maxAmount;

  @NotNull
  private Long minTerm;

  @NotNull
  private Long maxTerm;

  @NotNull
  private Integer minHour;

  @NotNull
  private Integer maxHour;

  @NotNull
  private Integer rate;

  @NotNull
  private Long extensionTermInDays;

  @NotNull
  private List<String> supportedCurrencies;
}
