package com.tgelo.loanservice.business;

import com.tgelo.loanservice.domain.error.Error;
import com.tgelo.loanservice.domain.offer.LoanOffer;
import io.vavr.collection.List;
import io.vavr.control.Validation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.Currency;

import static io.vavr.API.Valid;

@Service
@Slf4j
public class LoanOfferOperations {

  private final DefaultLoanOfferConfiguration defaultLoanOfferConfiguration;

  public LoanOfferOperations(final DefaultLoanOfferConfiguration defaultLoanOfferConfiguration) {
    this.defaultLoanOfferConfiguration = defaultLoanOfferConfiguration;
  }

  Validation<Error, LoanOffer> getDefaultOffer() {
    log.debug("Retrieve default loan offer");
    return Valid(
      LoanOffer.builder()
        .minAmount(defaultLoanOfferConfiguration.getMinAmount())
        .maxAmount(defaultLoanOfferConfiguration.getMaxAmount())
        .minTerm(defaultLoanOfferConfiguration.getMinTerm())
        .maxTerm(defaultLoanOfferConfiguration.getMaxTerm())
        .minHour(LocalTime.of(defaultLoanOfferConfiguration.getMinHour(), 0))
        .maxHour(LocalTime.of(defaultLoanOfferConfiguration.getMaxHour(), 0))
        .rate(defaultLoanOfferConfiguration.getRate())
        .extendLoanTerm(defaultLoanOfferConfiguration.getExtensionTermInDays())
        .supportedCurrencies(List.ofAll(defaultLoanOfferConfiguration.getSupportedCurrencies()).map(Currency::getInstance))
        .build()
    );
  }
}
