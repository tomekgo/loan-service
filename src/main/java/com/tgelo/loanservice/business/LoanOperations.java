package com.tgelo.loanservice.business;

import com.tgelo.loanservice.business.repositories.LoanRepository;
import com.tgelo.loanservice.domain.TimeProvider;
import com.tgelo.loanservice.domain.error.Error;
import com.tgelo.loanservice.domain.loan.Loan;
import com.tgelo.loanservice.domain.loan.LoanAction;
import com.tgelo.loanservice.domain.offer.LoanOffer;
import com.tgelo.loanservice.persistence.InsertLoanData;
import com.tgelo.loanservice.persistence.InsertLoanHistoryItemData;
import io.vavr.control.Validation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.temporal.ChronoUnit;

@Service
@Slf4j
public class LoanOperations {

  private final LoanRepository loanRepository;
  private final LoanOfferOperations loanOfferOperations;
  private final LoanValidations loanValidations;

  LoanOperations(
    final LoanRepository loanRepository,
    final LoanOfferOperations loanOfferOperations,
    final LoanValidations loanValidations
  ) {
    this.loanRepository = loanRepository;
    this.loanOfferOperations = loanOfferOperations;
    this.loanValidations = loanValidations;
  }

  @Transactional
  public Validation<Error, Loan> create(final CreateLoanCommand command) {
    log.debug("Create loan: {}", command);
    return loanOfferOperations.getDefaultOffer()
      .flatMap(offer -> loanValidations.validateCreationCommand(command, offer).map(c -> toInsertData(c, offer)))
      .flatMap(loanRepository::insert);
  }

  private InsertLoanData toInsertData(final CreateLoanCommand command, final LoanOffer offer) {
    return InsertLoanData.builder()
      .action(LoanAction.CREATE)
      .term(command.getTerm())
      .amount(command.getAmount())
      .rate(offer.getRate())
      .currency(command.getCurrency())
      .createdAt(command.getCreatedAt())
      .build();
  }

  public Validation<Error, Loan> extend(final ExtendLoanCommand command) {
    log.debug("Extend loan with id: {}", command.getLoanId());
    return loanOfferOperations.getDefaultOffer().flatMap(offer ->
      loanRepository.retrieve(command.getLoanId())
        .flatMap(loanValidations::validateLoanToBeExtended)
        .map(c -> toInsertData(c, offer))
        .flatMap(loanRepository::insert)
    );
  }

  private InsertLoanHistoryItemData toInsertData(final Loan loan, final LoanOffer loanOffer) {
    return InsertLoanHistoryItemData.builder()
      .loanId(loan.getId())
      .action(LoanAction.EXTEND)
      .createdAt(TimeProvider.instantNow())
      .totalTerm(loan.totalTerm() + loanOffer.getExtendLoanTerm())
      .dueDate(loan.dueDate().plus(loanOffer.getExtendLoanTerm(), ChronoUnit.DAYS))
      .build();
  }
}
