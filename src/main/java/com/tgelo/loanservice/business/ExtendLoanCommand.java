package com.tgelo.loanservice.business;

import lombok.NonNull;
import lombok.Value;

@Value(staticConstructor = "of")
public class ExtendLoanCommand {

  @NonNull
  Long loanId;
}
