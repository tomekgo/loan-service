package com.tgelo.loanservice.business;

import com.tgelo.loanservice.domain.TimeProvider;
import com.tgelo.loanservice.domain.error.Error;
import com.tgelo.loanservice.domain.error.ErrorCode;
import com.tgelo.loanservice.domain.loan.Loan;
import com.tgelo.loanservice.domain.offer.LoanOffer;
import io.vavr.control.Validation;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalTime;

import static com.tgelo.loanservice.domain.TimeProvider.zoneId;
import static io.vavr.API.Invalid;
import static io.vavr.API.Valid;
import static java.time.LocalTime.ofInstant;

@Service
class LoanValidations {

  Validation<Error, CreateLoanCommand> validateCreationCommand(final CreateLoanCommand command, final LoanOffer offer) {
    if (command.getAmount() < offer.getMinAmount() || command.getAmount() > offer.getMaxAmount()) {
      return Invalid(
        ErrorCode.LOAN_BEYOND_AMOUNT_RANGE.asError(
          command.getAmount(),
          offer.getMinAmount(),
          offer.getMaxAmount()
        )
      );
    }

    if (command.getTerm() < offer.getMinTerm() || command.getTerm() > offer.getMaxTerm()) {
      return Invalid(
        ErrorCode.LOAN_BEYOND_TERM_RANGE.asError(
          command.getTerm(),
          offer.getMinTerm(),
          offer.getMaxTerm()
        )
      );
    }

    if (command.getAmount().equals(offer.getMaxAmount())
      && isBetween(command.getCreatedAt(), offer.getMinHour(), offer.getMaxHour())
    ) {
      return Invalid(
        ErrorCode.LOAN_BEYOND_TIME_RANGE_AND_AMOUNT_LIMIT.asError(
          offer.getMinHour(),
          offer.getMaxHour(),
          offer.getMaxAmount()
        )
      );
    }

    if (!offer.getSupportedCurrencies().contains(command.getCurrency())) {
      return Invalid(ErrorCode.CURRENCY_NOT_SUPPORTED.asError(command.getCurrency().getCurrencyCode()));
    }
    return Valid(command);
  }

  private static boolean isBetween(final Instant createdAt, final LocalTime from, final LocalTime to) {
    return ofInstant(createdAt, zoneId()).isBefore(from) || ofInstant(createdAt, zoneId()).isAfter(to);
  }


  Validation<Error, Loan> validateLoanToBeExtended(final Loan loan) {
    if (loan.dueDate().isBefore(TimeProvider.instantNow())) {
      return Invalid(ErrorCode.CANNOT_EXTEND_EXPIRED_LOAN.asError(loan.getId(), loan.dueDate()));
    }
    return Valid(loan);
  }
}
