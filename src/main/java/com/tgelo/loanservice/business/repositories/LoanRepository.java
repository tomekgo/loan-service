package com.tgelo.loanservice.business.repositories;

import com.tgelo.loanservice.domain.error.Error;
import com.tgelo.loanservice.domain.loan.Loan;
import com.tgelo.loanservice.persistence.InsertLoanData;
import com.tgelo.loanservice.persistence.InsertLoanHistoryItemData;
import io.vavr.control.Validation;

public interface LoanRepository {

  Validation<Error, Loan> insert(final InsertLoanData insertData);

  Validation<Error, Loan> insert(final InsertLoanHistoryItemData insertData);

  Validation<Error, Loan> retrieve(final Long id);

}
