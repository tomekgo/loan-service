package com.tgelo.loanservice.business;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.Wither;

import java.time.Instant;
import java.util.Currency;

@Builder
@Wither
@Value
public class CreateLoanCommand {

  @NonNull
  Long amount;

  @NonNull
  Currency currency;

  @NonNull
  Long term;

  @NonNull
  Instant createdAt;
}
