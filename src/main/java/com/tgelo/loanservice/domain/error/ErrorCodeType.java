package com.tgelo.loanservice.domain.error;

import org.springframework.http.HttpStatus;

public enum ErrorCodeType {

  BAD_REQUEST(HttpStatus.BAD_REQUEST),
  NOT_FOUND(HttpStatus.NOT_FOUND),
  INTERNAL(HttpStatus.INTERNAL_SERVER_ERROR),
  ;

  final HttpStatus status;

  ErrorCodeType(HttpStatus status) {
    this.status = status;
  }

  public int statusCode() {
    return status.value();
  }
}
