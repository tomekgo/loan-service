package com.tgelo.loanservice.domain.error;

import io.vavr.collection.List;

import java.text.MessageFormat;
import java.util.Objects;

public enum ErrorCode {

  // general errors
  INVALID_REQUEST(ErrorCodeType.BAD_REQUEST, "Bad request"),
  INTERNAL_ERROR(ErrorCodeType.INTERNAL, "Internal server error"),

  DATA_ACCESS_EXCEPTION(ErrorCodeType.INTERNAL, "Database access failed."),

  LOAN_DOES_NOT_EXIST(
    ErrorCodeType.NOT_FOUND,
    "Loan with given id: {0} does not exist."
  ),
  LOAN_BEYOND_AMOUNT_RANGE(
    ErrorCodeType.BAD_REQUEST,
    "Requested amount: {0} is beyond accepted range that is min: {1} and max: {0}."
  ),
  LOAN_BEYOND_TERM_RANGE(
    ErrorCodeType.BAD_REQUEST,
    "Requested totalTerm: {0} is beyond accepted range that is min: {1} and max: {0}."
  ),
  LOAN_BEYOND_TIME_RANGE_AND_AMOUNT_LIMIT(
    ErrorCodeType.BAD_REQUEST,
    "Request loan is beyond time range min: {0} and max {1} and amount limit: {2}."
  ),
  CANNOT_EXTEND_EXPIRED_LOAN(
    ErrorCodeType.BAD_REQUEST,
    "Cannot extend expired loan with id: {0} and due date: {1}."
  ),
  CURRENCY_NOT_SUPPORTED(
    ErrorCodeType.BAD_REQUEST,
    "Currency: {0} is not supported."
  );

  final ErrorCodeType type;
  private final String message;

  ErrorCode(final ErrorCodeType type, final String message) {
    this.type = type;
    this.message = message;
  }

  public Error asError(final Object... params) {
    return Error.builder()
      .type(type)
      .code(name())
      .arguments(List.of(params).map(Objects::toString))
      .message(MessageFormat.format(message, params))
      .build();
  }
}
