package com.tgelo.loanservice.domain.error;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

@Builder
@Wither
@Value
public class Error {

  private ErrorCodeType type;

  private String code;

  private List<String> arguments;

  private String message;

  public int getStatus() {
    return type.statusCode();
  }
}
