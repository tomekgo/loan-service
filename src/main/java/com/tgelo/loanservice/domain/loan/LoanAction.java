package com.tgelo.loanservice.domain.loan;

public enum LoanAction {

  CREATE, EXTEND
}
