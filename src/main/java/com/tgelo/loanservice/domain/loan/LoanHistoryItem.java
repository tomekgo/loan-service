package com.tgelo.loanservice.domain.loan;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import java.time.Instant;

@Builder
@Wither
@Value
public class LoanHistoryItem {

  Long id;

  Instant dueDate;

  Long totalTerm;

  LoanAction action;

  Long loanId;

  Instant createdAt;
}
