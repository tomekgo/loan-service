package com.tgelo.loanservice.domain.loan;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Currency;

@Builder
@Wither
@Value
public class Loan {

  Long id;

  Long amount;

  Integer rate;

  Currency currency;

  Instant createdAt;

  List<LoanHistoryItem> history;

  public Long cost() {
    return amount * rate / 100L;
  }

  public Long totalTerm() {
    return history.lastOption().map(LoanHistoryItem::getTotalTerm).getOrElse(0L);
  }

  public Instant dueDate() {
    return createdAt.plus(totalTerm(), ChronoUnit.DAYS);
  }
}
