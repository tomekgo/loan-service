package com.tgelo.loanservice.domain.offer;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import java.time.LocalTime;
import java.util.Currency;

@Builder
@Wither
@Value
public class LoanOffer {

  private Long minAmount;

  private Long maxAmount;

  private Long minTerm;

  private Long maxTerm;

  private LocalTime minHour;

  private LocalTime maxHour;

  private Integer rate;

  private Long extendLoanTerm;

  private List<Currency> supportedCurrencies;
}
