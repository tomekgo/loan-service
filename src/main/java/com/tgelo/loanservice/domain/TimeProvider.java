package com.tgelo.loanservice.domain;

import lombok.experimental.UtilityClass;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

@UtilityClass
public class TimeProvider {

  private static final ZoneId ZONE_ID = TimeZone.getTimeZone("UTC").toZoneId();
  private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ISO_INSTANT;

  public static ZoneId zoneId() {
    return ZONE_ID;
  }

  public static ZonedDateTime zonedNow() {
    return ZonedDateTime.now(ZONE_ID);
  }

  public static Instant instantNow() {
    return ZonedDateTime.now(ZONE_ID).toInstant();
  }

  public static DateTimeFormatter dateFormat() {
    return DATE_FORMAT;
  }
}
