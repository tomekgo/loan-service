DROP TABLE IF EXISTS
loans,
loan_history_items;

DROP SEQUENCE IF EXISTS
seq_loan_id,
seq_loan_history_item_id;

-- ************************************** loans
CREATE SEQUENCE seq_loan_id START 1;

CREATE TABLE loans
(
 id             BIGINT NOT NULL DEFAULT NEXTVAL('seq_loan_id'),
 amount          BIGINT NOT NULL,
 cost      BIGINT NOT NULL,
 rate      INT NOT NULL,
 currency  TEXT NOT NULL,
 created_at      TIMESTAMP NOT NULL
);

ALTER TABLE loans
  ADD CONSTRAINT pk_loans
  PRIMARY KEY (id);

-- ************************************** loans-history
CREATE SEQUENCE seq_loan_history_item_id START 1;

CREATE TABLE loan_history_items
(
 id          BIGINT NOT NULL DEFAULT NEXTVAL('seq_loan_history_item_id'),
 loan_id  BIGINT NOT NULL,
 action  TEXT NOT NULL,
 total_term         BIGINT NOT NULL,
 due_date TIMESTAMP NOT NULL,
 created_at      TIMESTAMP NOT NULL
);

ALTER TABLE loan_history_items
  ADD CONSTRAINT pk_loan_history_items
  PRIMARY KEY (id);
ALTER TABLE loan_history_items
  ADD CONSTRAINT fk_loan_loan_history_items
  FOREIGN KEY (loan_id)
  REFERENCES loans (id);
