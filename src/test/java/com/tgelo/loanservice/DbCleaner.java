package com.tgelo.loanservice;

import io.vavr.collection.List;
import org.jooq.DSLContext;
import org.jooq.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.tgelo.loanservice.Tables.LOAN_HISTORY_ITEMS;
import static com.tgelo.loanservice.tables.Loans.LOANS;

@Component
public class DbCleaner {

  @Autowired
  private DSLContext context;

  public void execute() {
    List.of(
      LOAN_HISTORY_ITEMS,
      LOANS
    )
      .map(context::deleteFrom)
      .forEach(Query::execute);
  }
}
