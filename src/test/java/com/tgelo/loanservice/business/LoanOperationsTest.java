package com.tgelo.loanservice.business;

import com.tgelo.loanservice.business.repositories.LoanRepository;
import com.tgelo.loanservice.domain.TimeProvider;
import com.tgelo.loanservice.domain.error.Error;
import com.tgelo.loanservice.domain.error.ErrorCode;
import com.tgelo.loanservice.domain.loan.Loan;
import com.tgelo.loanservice.domain.loan.LoanAction;
import com.tgelo.loanservice.domain.offer.LoanOffer;
import com.tgelo.loanservice.persistence.InsertLoanData;
import com.tgelo.loanservice.persistence.InsertLoanHistoryItemData;
import io.vavr.control.Validation;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Currency;

import static com.tgelo.loanservice.LoanFixtures.someCreateLoanCommand;
import static com.tgelo.loanservice.LoanFixtures.someLoan;
import static com.tgelo.loanservice.LoanFixtures.someLoanOffer;
import static com.tgelo.loanservice.domain.TimeProvider.zoneId;
import static com.tgelo.loanservice.domain.error.ErrorCode.LOAN_DOES_NOT_EXIST;
import static io.vavr.API.Invalid;
import static io.vavr.API.Valid;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

class LoanOperationsTest {

  private final LoanRepository loanRepository = mock(LoanRepository.class);
  private final LoanOfferOperations loanOfferOperations = mock(LoanOfferOperations.class);
  private final LoanValidations loanValidations = mock(LoanValidations.class);

  private final LoanOperations loanOperations = new LoanOperations(
    loanRepository,
    loanOfferOperations,
    loanValidations
  );

  @Test
  void thatLoanCanBeCreated() {
    //given
    final CreateLoanCommand command = CreateLoanCommand.builder()
      .term(10L)
      .currency(Currency.getInstance("PLN"))
      .amount(500L)
      .createdAt(ZonedDateTime.of(2018, 5, 5, 5, 0, 0, 0, zoneId()).toInstant())
      .build();

    final Loan expectedLoan = someLoan();
    final LoanOffer someLoanOffer = someLoanOffer();

    when(loanOfferOperations.getDefaultOffer()).thenReturn(
      Valid(someLoanOffer)
    );

    when(loanValidations.validateCreationCommand(any(), any())).thenReturn(
      Valid(command)
    );

    when(loanRepository.insert(any(InsertLoanData.class))).thenReturn(Valid(expectedLoan));

    //when
    final Validation<Error, Loan> result = loanOperations.create(command);

    //then
    assertThat(result).containsExactly(
      expectedLoan
    );

    verify(loanOfferOperations).getDefaultOffer();

    verify(loanValidations).validateCreationCommand(command, someLoanOffer);

    verify(loanRepository).insert(
      InsertLoanData.builder()
        .action(LoanAction.CREATE)
        .term(command.getTerm())
        .amount(command.getAmount())
        .rate(someLoanOffer.getRate())
        .currency(command.getCurrency())
        .createdAt(command.getCreatedAt())
        .build()
    );
  }

  @Test
  void thatInvalidLoanCannotBeCreated() {
    //given
    final CreateLoanCommand command = someCreateLoanCommand();
    final LoanOffer loanOffer = someLoanOffer();
    final Error error = ErrorCode.LOAN_BEYOND_AMOUNT_RANGE.asError();

    when(loanOfferOperations.getDefaultOffer()).thenReturn(
      Valid(loanOffer)
    );

    when(loanValidations.validateCreationCommand(any(), any())).thenReturn(
      Invalid(error)
    );

    //when
    final Validation<Error, Loan> result = loanOperations.create(command);

    //then
    assertThat(result.getError()).isEqualTo(error);
    verify(loanValidations).validateCreationCommand(command, loanOffer);
    verifyNoMoreInteractions(loanRepository);
  }

  @Test
  void thatLoanCanBeExtended() {
    //given
    final Loan loan = someLoan();

    final ExtendLoanCommand command = ExtendLoanCommand.of(loan.getId());

    when(loanRepository.retrieve(any())).thenReturn(Valid(loan));
    when(loanRepository.insert(any(InsertLoanHistoryItemData.class))).thenReturn(Valid(loan));
    when(loanValidations.validateLoanToBeExtended(any())).thenReturn(Valid(loan));
    when(loanOfferOperations.getDefaultOffer()).thenReturn(
      Valid(someLoanOffer())
    );

    //when
    final Instant before = TimeProvider.instantNow();
    final Validation<Error, Loan> result = loanOperations.extend(command);
    final Instant after = TimeProvider.instantNow();

    //then
    assertThat(result).containsExactly(
      loan
    );
    final ArgumentCaptor<InsertLoanHistoryItemData> captor = ArgumentCaptor.forClass(InsertLoanHistoryItemData.class);

    verify(loanRepository).retrieve(command.getLoanId());
    verify(loanRepository).insert(captor.capture());
    verify(loanValidations).validateLoanToBeExtended(loan);
    assertThat(captor.getValue().getCreatedAt()).isBetween(
      before,
      after
    );

    assertThat(captor.getValue()).isEqualTo(
      InsertLoanHistoryItemData.builder()
        .loanId(loan.getId())
        .action(LoanAction.EXTEND)
        .createdAt(captor.getValue().getCreatedAt())
        .totalTerm(loan.getHistory().head().getTotalTerm() + 5L)
        .dueDate(loan.dueDate().plus(5L, ChronoUnit.DAYS))
        .build()
    );
  }

  @Test
  void thatNonexistentLoanCannotBeExtended() {
    //given
    final Long nonexistentLoanId = 1L;
    when(loanOfferOperations.getDefaultOffer()).thenReturn(Valid(someLoanOffer()));
    when(loanRepository.retrieve(any())).thenReturn(Invalid(LOAN_DOES_NOT_EXIST.asError(nonexistentLoanId)));

    //when
    final Validation<Error, Loan> result = loanOperations.extend(ExtendLoanCommand.of(nonexistentLoanId));

    //then
    assertThat(result.getError()).isEqualTo(LOAN_DOES_NOT_EXIST.asError(nonexistentLoanId));
  }


  @Test
  void thatInvalidLoanCannotBeExtended() {
    //given
    final Loan loan = someLoan();
    final Error error = ErrorCode.LOAN_BEYOND_TIME_RANGE_AND_AMOUNT_LIMIT.asError();

    when(loanOfferOperations.getDefaultOffer()).thenReturn(
      Valid(someLoanOffer())
    );
    when(loanRepository.retrieve(any())).thenReturn(Valid(loan));
    when(loanValidations.validateLoanToBeExtended(any())).thenReturn(
      Invalid(error)
    );

    //when
    final Validation<Error, Loan> result = loanOperations.extend(ExtendLoanCommand.of(loan.getId()));

    //then
    assertThat(result.getError()).isEqualTo(error);
    verify(loanValidations).validateLoanToBeExtended(loan);
    verify(loanRepository, times(0)).insert(any(InsertLoanHistoryItemData.class));
  }
}
