package com.tgelo.loanservice.business;

import com.tgelo.loanservice.domain.error.Error;
import com.tgelo.loanservice.domain.loan.Loan;
import com.tgelo.loanservice.domain.offer.LoanOffer;
import io.vavr.collection.List;
import io.vavr.control.Validation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Currency;
import java.util.stream.Stream;

import static com.tgelo.loanservice.LoanFixtures.someCreateLoanCommand;
import static com.tgelo.loanservice.LoanFixtures.someLoan;
import static com.tgelo.loanservice.LoanFixtures.someLoanOffer;
import static com.tgelo.loanservice.domain.TimeProvider.instantNow;
import static com.tgelo.loanservice.domain.TimeProvider.zoneId;
import static com.tgelo.loanservice.domain.error.ErrorCode.CANNOT_EXTEND_EXPIRED_LOAN;
import static com.tgelo.loanservice.domain.error.ErrorCode.CURRENCY_NOT_SUPPORTED;
import static com.tgelo.loanservice.domain.error.ErrorCode.LOAN_BEYOND_AMOUNT_RANGE;
import static com.tgelo.loanservice.domain.error.ErrorCode.LOAN_BEYOND_TERM_RANGE;
import static com.tgelo.loanservice.domain.error.ErrorCode.LOAN_BEYOND_TIME_RANGE_AND_AMOUNT_LIMIT;
import static org.assertj.core.api.Assertions.assertThat;

class LoanValidationsTest {

  private final LoanValidations validations = new LoanValidations();

  @ParameterizedTest
  @MethodSource("invalidLoanCreationTestData")
  void thatInvalidLoanCreationReturnsError(
    final LoanOffer loanOffer,
    final CreateLoanCommand command,
    final Error expectedError
  ) {
    //when
    final Validation<Error, CreateLoanCommand> result = validations.validateCreationCommand(command, loanOffer);

    //then
    assertThat(result.getError()).isEqualTo(expectedError);
  }

  private static Stream<Arguments> invalidLoanCreationTestData() {
    return Stream.of(
      Arguments.of(
        someLoanOffer().withMinAmount(100L).withMaxAmount(200L),
        someCreateLoanCommand().withAmount(50L),
        LOAN_BEYOND_AMOUNT_RANGE.asError(50L, 100L, 200L)
      ),
      Arguments.of(
        someLoanOffer().withMinAmount(100L).withMaxAmount(200L),
        someCreateLoanCommand().withAmount(250L),
        LOAN_BEYOND_AMOUNT_RANGE.asError(250L, 100L, 200L)
      ),
      Arguments.of(
        someLoanOffer().withMinTerm(100L).withMaxTerm(200L),
        someCreateLoanCommand().withTerm(50L),
        LOAN_BEYOND_TERM_RANGE.asError(50L, 100L, 200L)
      ),
      Arguments.of(
        someLoanOffer().withMinTerm(100L).withMaxTerm(200L),
        someCreateLoanCommand().withTerm(250L),
        LOAN_BEYOND_TERM_RANGE.asError(250L, 100L, 200L)
      ),
      Arguments.of(
        someLoanOffer().withSupportedCurrencies(List.empty()),
        someCreateLoanCommand().withCurrency(Currency.getInstance("PLN")),
        CURRENCY_NOT_SUPPORTED.asError("PLN")
      ),
      Arguments.of(
        someLoanOffer().withMaxAmount(200L).withMinHour(LocalTime.of(10, 0)).withMaxHour(LocalTime.of(12, 0)),
        someCreateLoanCommand().withAmount(200L).withCreatedAt(ZonedDateTime.of(2018, 5, 5, 5, 0, 0, 0, zoneId()).toInstant()),
        LOAN_BEYOND_TIME_RANGE_AND_AMOUNT_LIMIT.asError(LocalTime.of(10, 0), LocalTime.of(12, 0), 200L)
      ),
      Arguments.of(
        someLoanOffer().withMaxAmount(200L).withMinHour(LocalTime.of(10, 0)).withMaxHour(LocalTime.of(12, 0)),
        someCreateLoanCommand().withAmount(200L).withCreatedAt(ZonedDateTime.of(2018, 5, 5, 13, 0, 0, 0, zoneId()).toInstant()),
        LOAN_BEYOND_TIME_RANGE_AND_AMOUNT_LIMIT.asError(LocalTime.of(10, 0), LocalTime.of(12, 0), 200L)
      )
    );
  }

  @Test
  void thatExpiredLoanCannotBeExtended() {
    //given
    final Loan loan = someLoan().withCreatedAt(instantNow().minus(100, ChronoUnit.DAYS));

    //when
    final Validation<Error, Loan> result = validations.validateLoanToBeExtended(loan);

    //then
    assertThat(result.getError()).isEqualTo(
      CANNOT_EXTEND_EXPIRED_LOAN.asError(loan.getId(), loan.dueDate())
    );
  }
}
