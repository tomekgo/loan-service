package com.tgelo.loanservice.business;

import com.tgelo.loanservice.domain.error.Error;
import com.tgelo.loanservice.domain.offer.LoanOffer;
import io.vavr.control.Validation;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.Currency;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class LoanOfferOperationsTest {

  private final DefaultLoanOfferConfiguration configuration = mock(DefaultLoanOfferConfiguration.class);
  private final LoanOfferOperations operations = new LoanOfferOperations(configuration);

  @Test
  void thatDefaultLoanOfferCanBeRetrieved() {
    when(configuration.getMinAmount()).thenReturn(1L);
    when(configuration.getMaxAmount()).thenReturn(2L);
    when(configuration.getMinTerm()).thenReturn(3L);
    when(configuration.getMaxTerm()).thenReturn(4L);
    when(configuration.getMinHour()).thenReturn(5);
    when(configuration.getMaxHour()).thenReturn(6);
    when(configuration.getRate()).thenReturn(7);
    when(configuration.getExtensionTermInDays()).thenReturn(8L);
    when(configuration.getSupportedCurrencies()).thenReturn(List.of("PLN"));

    final Validation<Error, LoanOffer> result = operations.getDefaultOffer();

    assertThat(result).containsExactly(
      LoanOffer.builder()
        .minAmount(1L)
        .maxAmount(2L)
        .minTerm(3L)
        .maxTerm(4L)
        .minHour(LocalTime.of(5, 0))
        .maxHour(LocalTime.of(6, 0))
        .rate(7)
        .extendLoanTerm(8L)
        .supportedCurrencies(io.vavr.collection.List.of(Currency.getInstance("PLN")))
        .build()
    );
  }
}
