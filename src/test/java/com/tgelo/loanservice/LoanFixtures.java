package com.tgelo.loanservice;

import com.tgelo.loanservice.business.CreateLoanCommand;
import com.tgelo.loanservice.domain.TimeProvider;
import com.tgelo.loanservice.domain.loan.Loan;
import com.tgelo.loanservice.domain.loan.LoanAction;
import com.tgelo.loanservice.domain.loan.LoanHistoryItem;
import com.tgelo.loanservice.domain.offer.LoanOffer;
import com.tgelo.loanservice.persistence.InsertLoanData;
import io.vavr.collection.List;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Currency;
import java.util.Locale;

import static com.tgelo.loanservice.domain.TimeProvider.instantNow;
import static com.tgelo.loanservice.domain.TimeProvider.zoneId;

public class LoanFixtures {

  public static Loan someLoan() {
    return Loan.builder()
      .id(1L)
      .createdAt(TimeProvider.instantNow())
      .rate(10)
      .amount(20L)
      .currency(Currency.getInstance("PLN"))
      .history(List.of(someLoanHistoryItem()))
      .build();
  }

  private static LoanHistoryItem someLoanHistoryItem() {
    return LoanHistoryItem.builder()
      .id(2L)
      .loanId(3L)
      .totalTerm(10L)
      .action(LoanAction.CREATE)
      .createdAt(TimeProvider.instantNow())
      .dueDate(TimeProvider.instantNow())
      .build();
  }

  public static InsertLoanData someLoanInsertData() {
    return InsertLoanData.builder()
      .amount(1L)
      .rate(10)
      .term(5L)
      .createdAt(instantNow())
      .currency(Currency.getInstance(Locale.UK))
      .action(LoanAction.CREATE)
      .build();
  }

  public static LoanOffer someLoanOffer() {
    return LoanOffer.builder()
      .minAmount(100L)
      .maxAmount(1000L)
      .minTerm(5L)
      .maxTerm(10L)
      .extendLoanTerm(5L)
      .rate(10)
      .minHour(LocalTime.of(0, 0))
      .maxHour(LocalTime.of(6, 0))
      .supportedCurrencies(List.of(Currency.getInstance("PLN")))
      .build();
  }

  public static CreateLoanCommand someCreateLoanCommand() {
    return CreateLoanCommand.builder()
      .term(10L)
      .currency(Currency.getInstance("PLN"))
      .amount(500L)
      .createdAt(ZonedDateTime.of(2018, 5, 5, 5, 0, 0, 0, zoneId()).toInstant())
      .build();
  }
}
