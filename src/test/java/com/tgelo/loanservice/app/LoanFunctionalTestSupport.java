package com.tgelo.loanservice.app;

import com.tgelo.loanservice.api.dto.CreateLoanRequest;
import com.tgelo.loanservice.api.dto.ErrorResponse;
import com.tgelo.loanservice.api.dto.LoanResponse;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.springframework.http.HttpStatus;

import static com.tgelo.loanservice.api.rest.LoanController.API_V1_LOANS;
import static com.tgelo.loanservice.api.rest.LoanController.API_V1_LOAN_EXTEND;

public interface LoanFunctionalTestSupport {

  RequestSpecification requestSpecification();

  default LoanResponse createLoan(final CreateLoanRequest request) {
    return requestSpecification()
      .body(request)
      .when()
      .post(API_V1_LOANS)
      .then()
      .log().all()
      .statusCode(HttpStatus.CREATED.value())
      .contentType(ContentType.JSON)
      .and()
      .extract()
      .body()
      .as(LoanResponse.class);
  }

  default ErrorResponse createLoanExpectingError(final Object request, final HttpStatus status) {
    return requestSpecification()
      .body(request)
      .when()
      .post(API_V1_LOANS)
      .then()
      .log().all()
      .statusCode(status.value())
      .contentType(ContentType.JSON)
      .and()
      .extract()
      .body()
      .as(ErrorResponse.class);
  }

  default LoanResponse extendLoan(final Long loanId) {
    return requestSpecification()
      .when()
      .post(API_V1_LOAN_EXTEND, loanId)
      .then()
      .log().all()
      .statusCode(HttpStatus.OK.value())
      .contentType(ContentType.JSON)
      .and()
      .extract()
      .body()
      .as(LoanResponse.class);
  }

  default ErrorResponse extendLoanExpectingError(final Long loanId, final HttpStatus status) {
    return requestSpecification()
      .post(API_V1_LOAN_EXTEND, loanId)
      .then()
      .log().all()
      .statusCode(status.value())
      .contentType(ContentType.JSON)
      .and()
      .extract()
      .body()
      .as(ErrorResponse.class);
  }
}
