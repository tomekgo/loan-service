package com.tgelo.loanservice.app;

import com.tgelo.loanservice.api.dto.CreateLoanRequest;
import com.tgelo.loanservice.api.dto.ErrorResponse;
import com.tgelo.loanservice.api.dto.LoanHistoryItemResponse;
import com.tgelo.loanservice.api.dto.LoanResponse;
import com.tgelo.loanservice.domain.error.ErrorCode;
import com.tgelo.loanservice.domain.loan.LoanAction;
import io.restassured.specification.RequestSpecification;
import io.vavr.collection.List;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.Currency;

import static com.tgelo.loanservice.domain.loan.LoanAction.EXTEND;
import static java.time.temporal.ChronoUnit.DAYS;
import static org.assertj.core.api.Assertions.assertThat;

public class LoanFunctionalTest extends CommonFunctionalTest implements LoanFunctionalTestSupport {

  @Override
  public RequestSpecification requestSpecification() {
    return super.requestSpecification();
  }

  @Test
  void thatLoanCanBeCreated() {
    final CreateLoanRequest request = CreateLoanRequest.builder()
      .amount(100L)
      .currency(Currency.getInstance("PLN"))
      .term(10L)
      .build();

    final LoanResponse result = createLoan(request);

    assertThat(result).isEqualToIgnoringGivenFields(
      LoanResponse.builder()
        .id(result.getId())
        .amount(100L)
        .cost(10L)
        .rate(10)
        .currency(Currency.getInstance("PLN"))
        .createdAt(result.getCreatedAt())
        .totalTerm(10L)
        .dueDate(result.getDueDate())
        .history(List.of(
          LoanHistoryItemResponse.builder()
            .id(result.getHistory().head().getId())
            .loanId(result.getId())
            .totalTerm(10L)
            .action(LoanAction.CREATE)
            .createdAt(result.getCreatedAt())
            .dueDate(result.getCreatedAt().plus(10L, DAYS))
            .build()
        ))
        .build()
    );
  }

  @Test
  void thatInvalidLoanCannotBeCreated() {
    final CreateLoanRequest request = CreateLoanRequest.builder()
      .amount(1L)
      .currency(Currency.getInstance("PLN"))
      .term(10L)
      .build();

    final ErrorResponse result = createLoanExpectingError(request, HttpStatus.BAD_REQUEST);

    assertThat(result).isEqualTo(ErrorResponse.of(ErrorCode.LOAN_BEYOND_AMOUNT_RANGE.asError(1L, 100, 1000)));
  }

  @Test
  void thatLoanCanBeExtended() {
    //given
    final LoanResponse loan = createLoan(someLoanRequest());

    //when
    final LoanResponse result = extendLoan(loan.getId());

    //then
    assertThat(result.getHistory().size()).isEqualTo(2);

    assertThat(result.getHistory().get(1)).isEqualTo(
      LoanHistoryItemResponse.builder()
        .id(result.getHistory().last().getId())
        .loanId(loan.getId())
        .createdAt(result.getHistory().last().getCreatedAt())
        .dueDate(loan.getDueDate().plus(28, DAYS))
        .action(EXTEND)
        .totalTerm(loan.getTotalTerm() + 28)
        .build()
    );
  }

  @Test
  void thatLoanCanBeExtendedMultipleTimes() {
    //given
    final LoanResponse loan = createLoan(someLoanRequest());
    extendLoan(loan.getId());

    //when
    final LoanResponse result = extendLoan(loan.getId());

    //then
    assertThat(result.getHistory().size()).isEqualTo(3);

    assertThat(result.getHistory().get(2)).isEqualTo(
      LoanHistoryItemResponse.builder()
        .id(result.getHistory().last().getId())
        .loanId(loan.getId())
        .createdAt(result.getHistory().last().getCreatedAt())
        .dueDate(loan.getDueDate().plus(28 + 28, DAYS))
        .action(EXTEND)
        .totalTerm(loan.getTotalTerm() + 28 + 28)
        .build()
    );
  }

  @Test
  void thatNonexistentLoanCannotBeExtended() {
    final Long nonexistentLoanId = 1L;

    final ErrorResponse result = extendLoanExpectingError(nonexistentLoanId, HttpStatus.NOT_FOUND);

    assertThat(result).isEqualTo(ErrorResponse.of(ErrorCode.LOAN_DOES_NOT_EXIST.asError(nonexistentLoanId)));
  }

  private static CreateLoanRequest someLoanRequest() {
    return CreateLoanRequest.builder()
      .amount(100L)
      .currency(Currency.getInstance("PLN"))
      .term(10L)
      .build();
  }
}
