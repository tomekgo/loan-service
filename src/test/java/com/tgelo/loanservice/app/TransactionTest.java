package com.tgelo.loanservice.app;

import com.tgelo.loanservice.app.transaction.TransactionalRunner;
import com.tgelo.loanservice.business.repositories.LoanRepository;
import com.tgelo.loanservice.domain.error.Error;
import com.tgelo.loanservice.domain.error.ErrorCode;
import org.jooq.DSLContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.tgelo.loanservice.LoanFixtures.someLoanInsertData;
import static com.tgelo.loanservice.tables.Loans.LOANS;
import static io.vavr.API.Invalid;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

class TransactionTest extends CommonFunctionalTest {

  @Autowired
  private TransactionalRunner transactionalRunner;

  @Autowired
  private LoanRepository loanRepository;

  @Autowired
  private DSLContext context;

  @Test
  void thatTransactionIsCommitted() {
    transactionalRunner.doInTransaction(
      () -> loanRepository.insert(someLoanInsertData())
    );

    assertThat(countLoans()).isEqualTo(1);
  }

  @Test
  void thatTransactionIsRolledBackOnException() {
    final var someException = new RuntimeException("bam!");

    final var caught = catchThrowable(() ->
      transactionalRunner.doInTransaction(() -> {
        loanRepository.insert(someLoanInsertData());
        throw someException;
      })
    );

    assertThat(countLoans()).isEqualTo(0);
    assertThat(caught).isEqualTo(someException);
  }

  @Test
  void thatTransactionIsRolledWhenInvalidIsReturned() {
    final var expectedError = Invalid(someError());

    final var result = transactionalRunner.doInTransaction(() -> {
      loanRepository.insert(someLoanInsertData());
      return expectedError;
    });

    assertThat(countLoans()).isEqualTo(0);
    assertThat(result).isEqualTo(expectedError);
  }

  private Error someError() {
    return ErrorCode.INTERNAL_ERROR.asError();
  }

  private Integer countLoans() {
    return context.fetchCount(LOANS);
  }
}
