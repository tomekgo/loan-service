package com.tgelo.loanservice.app;

import com.tgelo.loanservice.DbCleaner;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(
  webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public abstract class CommonFunctionalTest {

  @LocalServerPort
  private int serverPort;

  @Autowired
  private DbCleaner cleaner;

  @BeforeEach
  void beforeEach() {
    cleaner.execute();
  }

  @BeforeAll
  public static void before() {
    Assertions.setAllowComparingPrivateFields(false);
    RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
  }

  public RequestSpecification requestSpecification() {
    return RestAssured
      .given()
      .log().all()
      .port(serverPort)
      .accept(ContentType.JSON)
      .contentType(ContentType.JSON);
  }
}
