package com.tgelo.loanservice;

import io.swagger.v3.core.util.Yaml;
import io.swagger.v3.parser.OpenAPIV3Parser;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

@Slf4j
public class MergeOpenApi {

  public static void main(final String[] args) throws IOException {
    try {
      final String inputFile = args[0];
      final File outputFile = new File(args[1]);
      if (outputFile.getParentFile().mkdirs()) {
        log.info("Created {}", outputFile.getParent());
      }
      Yaml.mapper()
        .writeValue(
          outputFile,
          new OpenAPIV3Parser().read(inputFile)
        );
    } catch (final Exception e) {
      log.error("Merging Open API specification failed", e);
      throw e;
    }
  }
}
