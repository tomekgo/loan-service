package com.tgelo.loanservice.persistence;

import com.tgelo.loanservice.DbCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jooq.JooqAutoConfiguration;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(
  classes = {
    DataSourceAutoConfiguration.class,
    FlywayAutoConfiguration.class,
    JooqAutoConfiguration.class,
    AbstractRepositoryTest.TestConfiguration.class,
  },
  initializers = ConfigFileApplicationContextInitializer.class
)
abstract class AbstractRepositoryTest {

  @Autowired
  private DbCleaner dbCleaner;

  @AfterEach
  void cleanUp() {
    dbCleaner.execute();
  }

  @Configuration
  @ComponentScan(basePackageClasses = JooqRepository.class)
  @Import({DbCleaner.class})
  static class TestConfiguration {
  }
}
