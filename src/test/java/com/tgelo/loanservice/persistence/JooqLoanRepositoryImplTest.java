package com.tgelo.loanservice.persistence;

import com.tgelo.loanservice.domain.error.Error;
import com.tgelo.loanservice.domain.error.ErrorCode;
import com.tgelo.loanservice.domain.loan.Loan;
import com.tgelo.loanservice.domain.loan.LoanAction;
import com.tgelo.loanservice.domain.loan.LoanHistoryItem;
import io.vavr.collection.List;
import io.vavr.control.Validation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.temporal.ChronoUnit;
import java.util.Currency;
import java.util.Locale;

import static com.tgelo.loanservice.LoanFixtures.someLoanInsertData;
import static com.tgelo.loanservice.domain.TimeProvider.instantNow;
import static com.tgelo.loanservice.domain.error.ErrorCode.LOAN_DOES_NOT_EXIST;
import static org.assertj.core.api.Assertions.assertThat;

class JooqLoanRepositoryImplTest extends AbstractRepositoryTest {

  @Autowired
  private JooqLoanRepositoryImpl loanRepository;

  @Test
  void thatLoanCanBeCreated() {
    final InsertLoanData insertData = someLoanInsertData();

    final Validation<Error, Loan> result = loanRepository.insert(insertData);

    assertThat(result.isValid()).isTrue();
    assertThat(result.get().getId()).isNotNull();
    assertThat(result.get().getHistory().size()).isOne();
    assertThat(result.get().getHistory().head().getId()).isNotNull();
    assertThat(result.get()).isEqualTo(
      Loan.builder()
        .id(result.get().getId())
        .createdAt(insertData.getCreatedAt())
        .rate(insertData.getRate())
        .amount(insertData.getAmount())
        .currency(insertData.getCurrency())
        .history(List.of(
          LoanHistoryItem.builder()
            .id(result.get().getHistory().head().getId())
            .loanId(result.get().getId())
            .dueDate(insertData.dueDate())
            .createdAt(insertData.getCreatedAt())
            .totalTerm(insertData.getTerm())
            .action(insertData.getAction())
            .build()
        ))
        .build()
    );
  }

  @Test
  void thatHistoryItemCanBeAddedToLoan() {
    final Loan loan = loanRepository.insert(someLoanInsertData()).get();
    final InsertLoanHistoryItemData insertData = InsertLoanHistoryItemData.builder()
      .loanId(loan.getId())
      .totalTerm(5L)
      .dueDate(loan.dueDate().plus(5L, ChronoUnit.DAYS))
      .createdAt(instantNow())
      .action(LoanAction.EXTEND)
      .build();

    final Validation<Error, Loan> result = loanRepository.insert(insertData);

    assertThat(result.isValid()).isTrue();
    assertThat(result.get().getId()).isNotNull();
    assertThat(result.get().getHistory().size()).isEqualTo(2);
    assertThat(result.get()).isEqualTo(
      loan
        .withHistory(
          loan.getHistory().append(
            LoanHistoryItem.builder()
              .id(result.get().getHistory().get(1).getId())
              .loanId(loan.getId())
              .totalTerm(insertData.getTotalTerm())
              .action(LoanAction.EXTEND)
              .createdAt(insertData.getCreatedAt())
              .dueDate(insertData.getDueDate())
              .build()
          )
        )
    );
  }

  @Test
  void thatHistoryItemCannotBeAddedToNonexistentLoan() {
    final Long nonExistentLoanId = 5L;
    final InsertLoanHistoryItemData insertData = InsertLoanHistoryItemData.builder()
      .loanId(nonExistentLoanId)
      .totalTerm(5L)
      .dueDate(instantNow())
      .createdAt(instantNow())
      .action(LoanAction.EXTEND)
      .build();

    final Validation<Error, Loan> result = loanRepository.insert(insertData);

    assertThat(result.isInvalid()).isTrue();
    assertThat(result.getError()).isEqualTo(
      LOAN_DOES_NOT_EXIST.asError(nonExistentLoanId)
    );
  }

  @Test
  void thatLoanCanBeRetrieved() {
    final Loan loan = loanRepository.insert(someLoanInsertData()).get();

    final Validation<Error, Loan> result = loanRepository.retrieve(loan.getId());

    assertThat(result).containsExactly(loan);
  }

  @Test
  void thatRetrievingNonexistentLoanReturnsError() {
    final Long nonexistentLoanId = 1L;

    final Validation<Error, Loan> result = loanRepository.retrieve(nonexistentLoanId);

    assertThat(result.getError()).isEqualTo(ErrorCode.LOAN_DOES_NOT_EXIST.asError(nonexistentLoanId));
  }
}
