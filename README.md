# Loan service

This is a simple implementation of service that allows applying for a loan.

## Implementation decisions
A loan is represented as a base class with history. This allows fulfilling immutability and stores historical data upon loan extensions like due date and term.

Monetary values are stored in two fields, “currency” (.e.g PLN, EUR, INR) and “value” holding integer 
- value in minor currency of specified currency (e.g. currency=”EUR”, value=200 represents 2 EUR, currency=”INR”, value=2000 represents 2 INR).

## Requirements
* docker compose - to host database
* lombok - to generate boilerplate code. Remember to enable annotation processing in IDE

## How to build
./gradlew build

## How to run tests
./gradlew test

## How to run
./gradlew bootRun

## Api documentation
Documentation is available at localhost:8080
I encourage to test API using embedded swagger.
